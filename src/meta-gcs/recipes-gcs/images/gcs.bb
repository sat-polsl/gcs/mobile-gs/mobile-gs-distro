SUMMARY = "GCS image"
LICENSE = "CLOSED"

inherit core-image image

IMAGE_FSTYPES += "tar.gz"

IMAGE_FEATURES = "\
    allow-empty-password \
    allow-root-login \
    empty-root-password \
    ssh-server-openssh \
"

IMAGE_INSTALL = "\
    kernel-modules \
    packagegroup-core-boot \
    linux-firmware-rtl8723 \
    systemd-conf \
    iw \
    wpa-supplicant \
    wireless-regdb-static \
    iptables \
    tailscale \
"

