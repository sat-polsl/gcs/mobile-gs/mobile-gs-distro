SUMMARY = "Tailscale VPN"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://src/${GO_IMPORT}/LICENSE;md5=a672713a9eb730050e491c92edf7984d"

inherit go-mod systemd

SRC_URI = "git://github.com/tailscale/tailscale;protocol=https;branch=main"
SRCREV = "30d9201a11ebc2e3a0f17bf8963956b77dadeb5d" 

GO_IMPORT = "tailscale.com"
GO_WORKDIR = "${GO_IMPORT}"
GO_INSTALL = "\
    ${GO_IMPORT}/cmd/tailscale \
    ${GO_IMPORT}/cmd/tailscaled \
"

do_compile() {
	${GO} install ${GOBUILDFLAGS} ${GO_INSTALL}
}

do_compile[network] = "1"

do_install() {
    install -d ${D}/${bindir}
    install -d ${D}/${sbindir}
    install ${B}/bin/linux_arm/tailscale ${D}/${bindir}/tailscale
    install ${B}/bin/linux_arm/tailscaled ${D}/${sbindir}/tailscaled
    if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'true', 'false', d)}; then
        install -d ${D}${sysconfdir}/default/
        install -m 0644 ${WORKDIR}/build/src/${GO_IMPORT}/cmd/tailscaled/tailscaled.defaults ${D}${sysconfdir}/default/tailscaled
        install -d ${D}${systemd_unitdir}/system
        install -m 0644 ${WORKDIR}/build/src/${GO_IMPORT}/cmd/tailscaled/tailscaled.service ${D}${systemd_unitdir}/system/tailscaled.service
        install -d ${D}${sysconfdir}/systemd/system/multi-user.target.wants/
        ln -s ${systemd_unitdir}/system/tailscaled.service ${D}${sysconfdir}/systemd/system/multi-user.target.wants/tailscaled.service
    fi
}

SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE:${PN} = "tailscaled.service"
SYSTEMD_AUTO_ENABLE = "enable"

