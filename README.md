# Mobile Ground Station Linux Distribution

## Requirements

- [kas](https://pypi.org/project/kas/)

## Building
Use `kas/local.yml.template` to create `kas/local.yml` and fill required variables.


```
kas checkout kas/local.yml
kas build kas/local.yml
```
